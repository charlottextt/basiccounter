﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterClass
{
    public class BasicCounterProgram
    {
        public static int total = 0;


        public static int Incrementation()
        {

            return ++total;
        }

        public static int Decrementation()
        {
            return --total;
        }

        public static int Remise()
        {
            total = 0;
            return total;
        }
    }
}
