﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterClass;

namespace BasicCounterTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestIncrementation()
        {
            BasicCounterProgram.total = 7;
            Assert.AreEqual(8, BasicCounterProgram.Incrementation());
            

            BasicCounterProgram.total = -30;
            Assert.AreEqual(-29, BasicCounterProgram.Incrementation());
           
        }

        [TestMethod]
        public void TestDecrementation()
        {
            BasicCounterProgram.total = 11;
            Assert.AreEqual(10, BasicCounterProgram.Decrementation());

            BasicCounterProgram.total = -23;
            Assert.AreEqual(-24, BasicCounterProgram.Decrementation());
        }

        [TestMethod]
        public void TestRemise()
        {
            BasicCounterProgram.total = 55;
            Assert.AreEqual(0, BasicCounterProgram.Remise());

            BasicCounterProgram.total = -1002;
            Assert.AreEqual(0, BasicCounterProgram.Remise());
        }

    }
}
